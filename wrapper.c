#include "falcon.h"
#include "inner.h"

#define MAX_LOGN 10

int falcon_wrapper_keygen(unsigned int logn, char *privkey, char *pubkey)
{
	shake256_context sc;
	unsigned char buf[FALCON_TMPSIZE_KEYGEN(MAX_LOGN)];
	
	shake256_init_prng_from_system(&sc);
	return falcon_keygen_make(&sc, logn, privkey, FALCON_PRIVKEY_SIZE(logn), pubkey, FALCON_PUBKEY_SIZE(logn), buf, FALCON_TMPSIZE_KEYGEN(logn));
}

int falcon_wrapper_sign(char *sig, size_t *sig_len, int sig_type, const char *privkey, size_t privkey_len, const char *data, size_t data_len)
{
	shake256_context sc;
	unsigned char buf[FALCON_TMPSIZE_SIGNDYN(MAX_LOGN)];
	
	shake256_init_prng_from_system(&sc);
	return falcon_sign_dyn(&sc, sig, sig_len, sig_type, privkey, privkey_len, data, data_len, buf, FALCON_TMPSIZE_SIGNDYN(MAX_LOGN));
}

int falcon_wrapper_verify(const char *sig, size_t sig_len, const char *pubkey, size_t pubkey_len, const char *data, size_t data_len)
{
	unsigned char buf[FALCON_TMPSIZE_VERIFY(MAX_LOGN)];
	
	return falcon_verify(sig, sig_len, 0, pubkey, pubkey_len, data, data_len, buf, FALCON_TMPSIZE_VERIFY(MAX_LOGN));
}
