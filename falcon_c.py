from ctypes import *
import os

libfalcon = CDLL(os.path.join(os.path.dirname(__file__), "falcon.so"))

falcon_512_PUBLICKEYBYTES = 897
falcon_512_SECRETKEYBYTES = 1281
falcon_512_BYTES = 809

FALCON_SIG_COMPRESSED = 1
FALCON_SIG_PADDED = 2
FALCON_SIG_CT = 3

falcon_wrapper_keygen = libfalcon.falcon_wrapper_keygen
falcon_wrapper_keygen.argtypes = [c_uint, c_char_p, c_char_p]

falcon_wrapper_sign = libfalcon.falcon_wrapper_sign
falcon_wrapper_sign.argtypes = [c_char_p, c_void_p, c_int, c_char_p, c_size_t, c_char_p, c_size_t]

falcon_wrapper_verify = libfalcon.falcon_wrapper_verify
falcon_wrapper_verify.argtypes = [c_char_p, c_size_t, c_char_p, c_size_t, c_char_p, c_size_t]
falcon_wrapper_verify.restype = c_int

def sign(sk, msg):
	sig = create_string_buffer(falcon_512_BYTES)
	siglen = c_size_t(falcon_512_BYTES)
	
	falcon_wrapper_sign(sig, byref(siglen), FALCON_SIG_COMPRESSED, sk, len(sk), msg, len(msg))
	return sig.raw[:siglen.value]

def verify(pk, msg, sig):
	res = falcon_wrapper_verify(sig, len(sig), pk, len(pk), msg, len(msg))

	return not bool(res)
